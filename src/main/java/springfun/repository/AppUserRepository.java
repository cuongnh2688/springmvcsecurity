/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package springfun.repository;

import org.springframework.data.jpa.repository.*;

import springfun.entity.*;

/**
 */
public interface AppUserRepository extends JpaRepository<AppUser, Integer>
{

}
