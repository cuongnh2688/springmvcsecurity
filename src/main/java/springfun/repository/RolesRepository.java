/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package springfun.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

import springfun.entity.*;

/**
 */
@Repository
public interface RolesRepository extends JpaRepository<Role, String>
{

}
