package springfun.dao;

import javax.persistence.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

import springfun.entity.*;

@Repository
@Transactional
public class AppUserDAO
{

	@Autowired
	private EntityManager entityManager;

	public AppUser findUserAccount(String userName)
	{
		try
		{
			String sql = "Select e from " + AppUser.class.getName() + " e "
				+ " Where e.userName = :userName ";

			Query query = entityManager.createQuery(sql, AppUser.class);
			query.setParameter("userName", userName);

			return (AppUser) query.getSingleResult();
		}
		catch (NoResultException e)
		{
			return null;
		}
	}

}
