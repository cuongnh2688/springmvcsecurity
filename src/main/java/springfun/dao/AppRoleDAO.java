package springfun.dao;

import java.util.*;

import javax.persistence.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

import springfun.entity.*;

@Repository
@Transactional
public class AppRoleDAO
{

	@Autowired
	private EntityManager entityManager;

	public List<String> getRoleNamesByUser(Integer userId)
	{
		try
		{
			List<String> roles = new ArrayList<String>();
			String sql = "SELECT ur FROM " + UserRole.class.getName() + " ur"
				+ " WHERE ur.appUser.userId = :userId ";
			Query query = entityManager.createQuery(sql, UserRole.class);
			query.setParameter("userId", userId);

			List<UserRole> userRoles = query.getResultList();
			if (userRoles == null)
			{
				return new ArrayList<String>();
			}
			for (UserRole userRole : userRoles)
			{
				roles.add(userRole.getRole().getRoleCode());
			}
			return roles;
		}
		catch (NoResultException e)
		{
			return new ArrayList<String>();
		}
	}

	public List<Role> getRoles()
	{
		try
		{
			String sql = "SELECT ur FROM " + Role.class.getName() + " ur";
			Query query = entityManager.createQuery(sql, Role.class);
			List<Role> roles = query.getResultList();
			if (roles == null)
			{
				return new ArrayList<Role>();
			}
			return roles;
		}
		catch (NoResultException e)
		{
			return new ArrayList<Role>();
		}
	}
}
