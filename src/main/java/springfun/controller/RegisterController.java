/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package springfun.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;

import springfun.bean.*;
import springfun.config.*;
import springfun.entity.*;
import springfun.repository.*;

/**
 */
@Controller
public class RegisterController
{
	@Autowired
	private RolesRepository rolesRepository;

	@Autowired
	private AppUserRepository appUserRepository;

	@Autowired
	private UserRoleRepository userRoleRepository;

	@RequestMapping(value = "/regist", method = RequestMethod.POST)
	public String registUser(@ModelAttribute("userBean")
	UserBean userBean)
	{
		//Save user
		AppUser user = appUserRepository.save(
			new AppUser(
				userBean.getUserName(),
				WebSecurityConfig.passwordEncoder().encode(userBean.getPassword())));

		// Save user and role
		List<String> rolesOfUser = userBean.getRoleCodes();
		if (rolesOfUser != null)
		{
			UserRole userRole;
			for (String roleCode : rolesOfUser)
			{
				Role role = rolesRepository.getById(roleCode);
				userRole = new UserRole(user, role);
				userRoleRepository.save(userRole);
			}
		}
		return "register/registSuccessfullPage";
	}

	@RequestMapping(value = "/prepareRegisting", method = RequestMethod.GET)
	public String prepareRegistingUser(Model model)
	{
		// Get list of role
		List<Role> roles = rolesRepository.findAll();
		model.addAttribute("roles", roles);

		// Prepare for new user
		UserBean userBean = new UserBean();
		model.addAttribute("userBean", userBean);

		return "register/registingUserPage";
	}
}
