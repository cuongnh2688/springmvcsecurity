package springfun.service;

import java.util.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.security.core.*;
import org.springframework.security.core.authority.*;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.*;

import springfun.dao.*;
import springfun.entity.*;

@Service
public class UserDetailsServiceImpl implements UserDetailsService
{

	@Autowired
	private AppUserDAO appUserDAO;

	@Autowired
	private AppRoleDAO appRoleDAO;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException
	{

		// Check exist user in database
		AppUser appUser = this.appUserDAO.findUserAccount(userName);
		if (appUser == null)
		{
			System.out.println("User not found! " + userName);
			throw new UsernameNotFoundException(
				"User " + userName + " was not found in the database");
		}

		// Get role name of user
		List<String> roleNames = this.appRoleDAO.getRoleNamesByUser(appUser.getUserId());

		// Add user name, password and role of user into spring security.
		List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
		for (String roleName : roleNames)
		{
			GrantedAuthority authority = new SimpleGrantedAuthority(roleName);
			grantList.add(authority);
		}
		return new User(appUser.getUserName(), appUser.getEncrytedPassword(), grantList);
	}

}
