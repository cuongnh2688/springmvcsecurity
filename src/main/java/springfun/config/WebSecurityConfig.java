package springfun.config;

import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.*;
import org.springframework.security.config.annotation.authentication.builders.*;
import org.springframework.security.config.annotation.web.builders.*;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.crypto.bcrypt.*;
import org.springframework.security.web.authentication.rememberme.*;

import springfun.service.*;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter
{

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception
	{
		auth.userDetailsService(userDetailsService).passwordEncoder(this.passwordEncoder());
	}

	@Bean
	/**
	 *
	 */
	//TODO if do not mark this method is static, it will throw exception. Occur with spring boot 2.6.2
	public static BCryptPasswordEncoder passwordEncoder()
	{
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception
	{

		// CSRF ( Cross Site Request Forgery) là kĩ thuật tấn công
		// bằng cách sử dụng quyền chứng thực của người sử dụng đối với 1 website khác
		http.csrf().disable();

		// Cac trang khong yeu cau login thi minh cho phep vao
		http.authorizeRequests().antMatchers("/", "/login", "/logout").permitAll();

		// Voi trang user info thi user co ROLE User va admin truy cap duoc
		http.authorizeRequests()
			.antMatchers("/userInfo")
			.access("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')");

		// Trang nay chi cho admin
		http.authorizeRequests()
			.antMatchers("/admin", "/regist", "/prepareRegisting")
			.access("hasRole('ROLE_ADMIN')");

		// Khi người dùng đã login, với vai trò user .
		// Nhưng cố ý truy cập vào trang admin
		// Ngoại lệ AccessDeniedException sẽ ném ra.
		// Ở đây mình tạo thêm một trang web lỗi tên 403.html (mọi người có thể tạo bất
		// cứ tên nào
		http.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403");

		// Cấu hình cho Login Form.
		http.authorizeRequests()
			.and()
			.formLogin()
			.loginProcessingUrl("/j_spring_security_check")
			.loginPage("/login")
			// đây Khi đăng nhập thành công thì vào trang này.
			// userAccountInfo sẽ được khai báo trong controller để hiển thị trang view tương ứng
			.defaultSuccessUrl("/userInfo")
			// Khi đăng nhập sai username và password thì nhập lại
			.failureUrl("/login?error=true")
			.usernameParameter("username")// tham số này nhận từ form login
			.passwordParameter("password")// tham số này nhận từ form login
			// Cấu hình cho Logout Page. Khi logout mình trả về trang
			.and()
			.logout()
			.logoutUrl("/logout")
			.logoutSuccessUrl("/logoutSuccessful");

		// Cấu hình Remember Me.
		// Ở form login bước 3, ta có 1 nút remember me.
		// Nếu người dùng tick vào đó ta sẽ dùng cookie lưu lại trong 24h
		http.authorizeRequests()
			.and()
			.rememberMe()
			.tokenRepository(this.persistentTokenRepository())
			.tokenValiditySeconds(24 * 60 * 60);// Luu 24h
	}

	@Bean
	public PersistentTokenRepository persistentTokenRepository()
	{
		// Ta lưu tạm remember me trong memory (RAM). Nếu cần mình có thể lưu trong database
		InMemoryTokenRepositoryImpl memory = new InMemoryTokenRepositoryImpl();
		return memory;
	}

}
