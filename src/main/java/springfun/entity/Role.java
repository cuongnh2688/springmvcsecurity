package springfun.entity;

import javax.persistence.*;

@Entity
@Table(name = "ROLE", uniqueConstraints = {
		@UniqueConstraint(name = "ROLE_UK", columnNames = "ROLE_NAME") })
public class Role
{

	@Id
	@Column(name = "ROLE_CODE", length = 45, nullable = false)
	private String roleCode;

	@Column(name = "ROLE_NAME", length = 45, nullable = false)
	private String roleName;

	public String getRoleCode()
	{
		return this.roleCode;
	}

	public void setRoleCode(String roleCode)
	{
		this.roleCode = roleCode;
	}

	public String getRoleName()
	{
		return this.roleName;
	}

	public void setRoleName(String roleName)
	{
		this.roleName = roleName;
	}
}
