package springfun.entity;

import javax.persistence.*;

@Entity
@Table(name = "APP_USER", uniqueConstraints = {
		@UniqueConstraint(name = "APP_USER_UK", columnNames = "USER_NAME") })
public class AppUser
{

	@Id
	@GeneratedValue
	@Column(name = "USER_ID", nullable = false)
	private Integer userId;

	@Column(name = "USER_NAME", length = 45, nullable = false)
	private String userName;

	@Column(name = "ENCRYED_PASSWORD", length = 300, nullable = false)
	private String encrytedPassword;

	@Column(name = "ENABLED", length = 1, nullable = false)
	private boolean enabled;

	public AppUser()
	{

	}

	public AppUser(String userName, String encrytedPassword)
	{
		this.userName = userName;
		this.encrytedPassword = encrytedPassword;
		this.enabled = true;

	}

	public Integer getUserId()
	{
		return userId;
	}
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	public String getUserName()
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	public String getEncrytedPassword()
	{
		return encrytedPassword;
	}
	public void setEncrytedPassword(String encrytedPassword)
	{
		this.encrytedPassword = encrytedPassword;
	}
	public boolean isEnabled()
	{
		return this.enabled;
	}
	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}
}
