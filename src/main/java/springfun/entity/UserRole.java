package springfun.entity;

import javax.persistence.*;

@Entity
@Table(name = "USER_ROLE", //
	uniqueConstraints = { //
			@UniqueConstraint(name = "USER_ROLE_UK", columnNames = { "USER_ID", "ROLE_CODE" }) })
public class UserRole
{

	@Id
	@GeneratedValue
	@Column(name = "ID", nullable = false)
	private Integer id;

	//	@Column(name = "USER_ID", nullable = false)
	//	private Integer userId;
	//
	//	@Column(name = "ROLE_CODE", length = 45, nullable = false)
	//	private String roleCode;

	@ManyToOne
	@JoinColumn(name = "USER_ID", nullable = false)
	private AppUser appUser;

	@ManyToOne
	@JoinColumn(name = "ROLE_CODE", nullable = false)
	private Role role;

	public UserRole()
	{

	}

	public UserRole(AppUser appUser, Role role)
	{
		this.appUser = appUser;
		this.role = role;
	}

	public Integer getId()
	{
		return this.id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public AppUser getAppUser()
	{
		return this.appUser;
	}

	public void setAppUser(AppUser appUser)
	{
		this.appUser = appUser;
	}

	public Role getRole()
	{
		return this.role;
	}

	public void setRole(Role role)
	{
		this.role = role;
	}

	//	public Integer getUserId()
	//	{
	//		return this.userId;
	//	}
	//
	//	public void setUserId(Integer userId)
	//	{
	//		this.userId = userId;
	//	}
	//
	//	public String getRoleCode()
	//	{
	//		return this.roleCode;
	//	}
	//
	//	public void setRoleCode(String roleCode)
	//	{
	//		this.roleCode = roleCode;
	//	}
}
