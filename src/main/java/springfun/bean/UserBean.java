/*
 * Copyright Orchestra Networks 2000-2010. All rights reserved.
 */
package springfun.bean;

import java.util.*;

/**
 */
public class UserBean
{
	private String userName;
	private String password;
	private List<String> roleCodes;

	public String getUserName()
	{
		return this.userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	public String getPassword()
	{
		return this.password;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}
	public List<String> getRoleCodes()
	{
		return this.roleCodes;
	}
	public void setRoleCodes(List<String> roleCodes)
	{
		this.roleCodes = roleCodes;
	}

}
