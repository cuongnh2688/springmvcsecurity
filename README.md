# SpBootFun

`[DESCRIBE PURPOSE OF YOUR PROJECT]`

#[[
- Java 8+
- Vert.x 3+

#[[
You can install this project through Maven's Central Repository. In your
`pom.xml`:

```
<dependencies>
  <dependency>
    <groupId>SpBoot</groupId>
    <artfiactId>SpBootFun</artifactId>
    <version>0.1.0-SNAPSHOT</version>
  </dependency>
</dependencies>
```

#[[
- Source: ${project.url}
- Documentation: [`/docs`](docs/)
- Issue Tracker: ${project.url}/issues